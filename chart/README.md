# Wikibase Helm Chart

Deploy your own private Wikibase instance with a SPARQL query service and QuickStatements.

Please also see https://gitlab.com/calincs/infrastructure/wikibase-chart

## Prerequisites

* Kubernetes 1.16+
* A persistent storage resource and RW access to it
* Kubernetes StorageClass for dynamic provisioning
* Nginx ingress controller with a load balancer set up

## Configuration

For a more robust solution supply helm install with a custom values.yaml

The following table lists common configurable parameters of the chart and
their default values. See values.yaml for all available options.

| Parameter                               | Description                                                        | Default                              |
|-----------------------------------------|--------------------------------------------------------------------|--------------------------------------|
| `domain`                                | Domain for your wikibase services                                  | `my-wb.example.com`                  |
| `storage.images`                        | Wikibase images Volume Size                                        | `5Gi`                                |
| `storage.database`                      | MySQL Volume Size                                                  | `5Gi`                                |
| `storage.wdqs`                          | Query Service Volume Size                                          | `5Gi`                                |
| `storage.quickstatements`               | QuickStatements Volume Size                                        | `5Gi`                                |

Specify each parameter using the `--set key=value[,key=value]` argument to
`helm install` or provide a `custom.yaml` containing the values.

## Installation

```shell
helm repo add wb-repo https://gitlab.com/api/v4/projects/18740921/packages/helm/stable
helm install --name my-wikibase -f custom.yaml wb-repo/wikibase
```

### Authentication

The default user is `Admin` and the password is `WikibaseAdmin`

## Uninstall

By default, a deliberate uninstall will result in the persistent volume
claim being deleted.

```shell
helm delete my-wikibase
```

To delete the deployment and its history:

```shell
helm delete --purge my-wikibase
```
