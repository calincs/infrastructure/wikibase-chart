# Wikibase for LINCS

A Docker Compose and Helm deployment of Wikimedia with Wikibase and extension(WDQS, ElasticSearch, and QuickStatements).

It was originally based on the `wmde` [docker-compose](https://github.com/wmde/wikibase-docker) project.

It is now kept in line with the official Wikibase [Docker deployment](https://www.mediawiki.org/wiki/Wikibase/Docker).

## Running Wikibase locally

Make sure you have Docker and Git installed and then clone the repo:

```bash
git clone git@gitlab.com:calincs/infrastructure/wikibase-chart.git
```

Then edit the values in [.env](./.env) to fit your environment. Then run:

```bash
docker compose up
```

Wikibase should be available at `http://localhost:8080`.

## Job runner

The example docker-compose.yml sets up a dedicated job runner which restarts itself after every job, to ensure that changes to the configuration are picked up as quickly as possible.

If you run large batches of edits, this job runner may not be able to keep up with edits. You can speed it up by increasing the `MAX_JOBS` variable to run more jobs between restarts, if you’re okay with configuration changes not taking effect in the job runner immediately. Alternatively, you can run several job runners in parallel by using the --scale option.

```bash
docker compose up --scale wikibase_jobrunner=8
```
